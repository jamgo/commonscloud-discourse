#!/bin/bash

set -x

mkdir -p /srv/docker-data/discourse/redis
mkdir -p /srv/docker-data/discourse/db
mkdir -p /srv/docker-data/discourse/main

docker volume create --name discourse-redis \
 --opt type=none \
 --opt device=/srv/docker-data/discourse/redis \
 --opt o=bind
docker volume create --name discourse-db \
 --opt type=none \
 --opt device=/srv/docker-data/discourse/db \
 --opt o=bind
docker volume create --name discourse-main \
 --opt type=none \
 --opt device=/srv/docker-data/discourse/main \
 --opt o=bind

docker-compose run --rm discourse bash -c "sleep 60 && rake db:migrate assets:precompile"
docker-compose run --rm discourse bash -c "rake admin:create"

docker-compose run --rm discourse bash -c "rake plugin:install repo=https://github.com/jonmbake/discourse-ldap-auth.git"
docker-compose run --rm discourse bash -c "rake assets:precompile"

export $(cat .env | xargs)
cat init.rb.template | envsubst | docker-compose run --rm discourse bundle exec rails c

docker-compose up --no-start
 
docker network connect bridge discourse
docker network connect discourse ldap